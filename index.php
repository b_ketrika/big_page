<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
   <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="./assets/css/style.css">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 block1 text-center">
                <i class="fa fa-home "></i> Home 
            </div>
            <div class="col-md-3 block2 text-center">
                <i class="fa fa-video-camera "></i> Video
            </div>
            <div class="col-md-3 block3 text-center">
                <i class="fa fa-music "></i> Music
            </div>
            <div class="col-md-3 block4 text-center ">
                <i class="fa fa-envelope "></i> Message
            </div>
        </div>
    </div>
   
    <script src="./assets/js/jquery.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
</body>
</html>
